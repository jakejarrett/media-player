import $ from "jquery";
import _ from "lodash";
import Radio from "backbone.radio";
import Backbone from "backbone";
import Marionette, { Application } from "marionette";
import LayoutView from "./layout_view";
import ComponentController from "modules/common/controllers/component-controller";
import TopBarView from "modules/views/topbar";
import SidebarView from "modules/views/sidebar";
import PlayerView from "modules/views/player";
import Mousetrap from "mousetrap";
import Router from "./routes";

/**
 * Setup radio channels
 */
let globalChannel = Radio.channel("app");
let routerChannel = Radio.channel("router");

/**
 * Setup the app & invoke it, then provide access to the invoked App variable.
 *
 * @type {Marionette.Application}
 * @module App
 * @namespace App
 */
let application = Marionette.Application.extend({
    /**
     * App region
     */
    region: "body",

    /**
     * When the DOM is shown, lets bind an event listener for the body click.
     */
    onShow () {
        /**
         * We should only have one body click event that is propagated via Backbone.Radio
         */
        $("body").on("click", (ev) => {

            /**
             * Triggers app:onBodyClick event
             *
             * @event app:onBodyClick
             * @memberof App
             * @example
             * var globalChannel = Backbone.Radio.channel("global");
             * var that = this;
             *
             * globalChannel.trigger("app:onBodyClick", (e) => {
             *      if(e.target.name === 'submit-form') {
             *          // Submit the form
             *      }
             * });
             */
            globalChannel.trigger("app:onBodyClick", ev);
        });
    }
});

/** Invoke Application **/
let App = new application();

/**
 * Start route controller and register a service worker
 */
App.on("start", () => {
    if (Backbone.history) {
        Backbone.history.start();
    }

    /**
     * Setup Service worker!
     */
    if("serviceWorker" in navigator) {
        // navigator.serviceWorker.register("/sw.js", { scope: "/" })
        //     .then(registration => console.log("Service Worker Registered"));
        //
        // navigator.serviceWorker.ready.then(registration => console.log("Service Worker Ready"));
    }

    /**
     * Hide loader
     */
    $(".loader").fadeOut();

	App.getTopbarContainer().show(new TopBarView);
	App.getSidebarContainer().show(new SidebarView);
	App.getMediaPlayerContainer().show(new PlayerView);

	/**
	 * Bind Control+Right arrow key for next song
	 */
	Mousetrap.bind("ctrl+right", event => {
		globalChannel.trigger("player:next:song");
	});

	/**
	 * Bind Control+Right arrow key for next song
	 */
	Mousetrap.bind("ctrl+left", event => {
		globalChannel.trigger("player:previous:song");
	});

	/**
	 * Bind space for toggle playback
	 */
	Mousetrap.bind("space", event => {
		event.preventDefault();
		globalChannel.trigger("player:toggle:playback");
	});

});

/**
 * Setup a region for the app & expose it on the App namespace
 * (Essentially providing a singleton)
 *
 * @protected
 */
App.layoutView = new LayoutView();
App.layoutView.render();

/**
 * Returns the navigation container
 *
 * @returns {HTMLElement} The navigation region
 * @public
 */
App.getSidebarContainer = () => App.layoutView.getRegion("sidebar");

/**
 * Returns the navigation container
 *
 * @returns {HTMLElement} The navigation region
 * @public
 */
App.getPlayerContainer = () => App.layoutView.getRegion("player");

/**
 * Returns the navigation container
 *
 * @returns {HTMLElement} The navigation region
 * @public
 */
App.getTopbarContainer = () => App.layoutView.getRegion("topbar");

/**
 * Returns the content container
 *
 * @returns {HTMLElement} The content region
 * @public
 */
App.getContentContainer = () => App.layoutView.getRegion("contentContainer");

/**
 * Returns the player container
 *
 * @returns {HTMLElement} The content region
 * @public
 */
App.getMediaPlayerContainer = () => App.layoutView.getRegion("player");

/**
 * Notify the application when the page has changed
 *
 * @event app:pageChange
 * @memberof App
 * @example
 * var globalChannel = Backbone.Radio.channel("global");
 * var that = this;
 *
 * // Clean up the dom when the page changes
 * globalChannel.trigger("app:pageChange", ()  => that.janitorialDuties());
 */
App.layoutView.on("empty", (view) => {
    globalChannel.trigger("app:pageChange");
    globalChannel.off("app:onBodyClick");
});

/**
 * Notify the application that the page will change.
 *
 * @event app:pageWillChange
 * @memberof App
 * @example
 * var globalChannel = Backbone.Radio.channel("global");
 * var that = this;
 *
 * // Clean up the dom before the page changes
 * globalChannel.trigger("app:pageWillChange", ()  => that.janitorialDuties());
 */
App.layoutView.on("before:empty", (view) => globalChannel.trigger("app:pageWillChange"));

/**
 * Provide a singleton component controller for the app.
 */
App.Compontents = new ComponentController;

App.radioChannel = globalChannel;

/**
 * Export the application
 *
 * @exports App
 */
export default App;
