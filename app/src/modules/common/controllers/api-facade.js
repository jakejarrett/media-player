import Local from "../services/local";
import { Model } from "backbone";

class API {

	__registeredServices = new Model();

	__currentService = "local";

	initialize () {
		const local = new Local();
		this.__registeredServices.set("local", { songs: local, title: local.title });
		this.__currentService = "local";
	}

	/**
	 * Get the registered services
	 * @returns {*}
	 */
	get registered () {
		return this.__registeredServices;
	}

	/**
	 * Gets the current service
	 */
	get currentService () {
		return this.__registeredServices.get(this.__currentService);
	}
}

export default API;
