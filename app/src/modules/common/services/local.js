import { app } from "electron";
import recursive from "recursive-readdir";
import homedir from "os-homedir";
import * as MusicMetadata from "musicmetadata";
import Radio from "backbone.radio";
import fs from "fs";

class Local {

	/**
	 * API service constructor
	 *
	 * @returns {Array} The list of songs
	 */
	constructor () {
		const that = this;
		this.__songs = [];

		this.__globalChannel = Radio.channel("app");

		const env = process.env;
		const home = env.HOME;
		const user = env.LOGNAME || env.USER || env.LNAME || env.USERNAME;

		let userHome = homedir();

		/**
		 * TODO- Make this dynamic
		 */
		const filesDone = new Promise((reject, resolve) => {
			let count = 0;
			let songs = [];

			recursive(`${userHome}/Music`, (err, files) => {
				console.log(files);
				files.forEach(file => {
					const fileStream = fs.createReadStream(file);

					MusicMetadata(fileStream, { duration: true }, (err, metadata) => {
						let artwork = `file://${__dirname}/assets/img/blank_artwork.png`;
						const durationAsSeconds = metadata.duration;

						if(err) {
							console.error(err);
							reject(err);
						}

						if (metadata.picture.length > 0) {
							let picture = metadata.picture[0];
							artwork = URL.createObjectURL(new Blob(
								[picture.data], {
									'type': `image/${picture.format}`
								}
							));
						}

						console.log();

						that.__songs.push({
							src: file,
							artist: metadata.artist[0] || "",
							title: metadata.title,
							album: metadata.album || "",
							albumArtist: metadata.albumArtist || "",
							genre: metadata.genre[0] || "",
							artwork: artwork,
							id: count,
							duration: durationAsSeconds
						});

						count++;

						fileStream.close();
					}, );
				});


				if(count === files.length) {
					resolve({songs: that.__songs});
				}

			});

		});

		filesDone.then(_ => {
			const songs = Object.assign(that.__songs, {});

			that.__globalChannel.trigger("songsLoaded", {
				API: "local",
				songs: songs
			});

			return this.__songs;
		});
	}

	get songs () {
		return this.__songs;
	}

	get title () {
		return "Local";
	}

}

export default Local;
