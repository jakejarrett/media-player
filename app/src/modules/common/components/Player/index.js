import { Component, on } from "@jakejarrett/marionette-component";
import { remote } from "electron";
import Radio from "backbone.radio";
import Template from "./index.html";
import Styles from "!css-loader?modules!sass-loader!./style.scss";

/**
 * The Sidebar component, handles anything that goes into the sidebar.
 */
class Player extends Component {

	__history = [];

	/**
	 * Component constructor, we run all our initialization here.
	 *
	 * @param elementName {String} The given element name
	 * @param props {Object} Render props, these are rendered with underscore's template method.
	 * @param localName {String} Passed from the app registration, used to prevent conflict with other elements.
	 * @returns {Player}
	 */
    constructor (elementName, props, localName) {
        super(elementName, localName);

		this.__globalChannel = Radio.channel("app");

        this.render(elementName, props);

        return this;
    }

	/**
	 * Render the component
	 *
	 * @param elementName {String} The element name
	 * @param props {Object} Properties that we render with (passed to _.template method)
	 */
	render (elementName, props) {
		const that = this;

        let data = {
            props: props
        };

        const renderedTemplate = _.template(Template)(data);

        this.renderComponent(elementName, renderedTemplate, Styles);

		this.__globalChannel.on("play:track", src => this.playTrack(src));
		this.__globalChannel.on("pause:track", src => this.pause());

		this.radioChannel.on("attached", event => {
			const player = this._element.shadowRoot.querySelector("audio");
			player.addEventListener("ended", event => this.__globalChannel.trigger("player:song:ended", this.__currentTrack));

			this.__globalChannel.trigger("player:song:ended", this.__currentTrack);

			this.__globalChannel.reply("player:track:current", () => {
				return this.__currentTrack;
			});

			this.__globalChannel.reply("player:track:previous", () => {
				return this.__history[this.__history.length - 2];
			});

			this.__globalChannel.on("player:toggle:playback", () => {
				this.togglePlayback();
			});
		});
    }

	/**
	 * When you click the Play button in the player controls
	 *
	 * @param e {Event} The click event
	 */
	@on("click #play")
	onPlayClick (e) {
		this.__globalChannel.trigger("player:toggle:playback");
	}

	/**
	 * Toggles playback
	 */
	togglePlayback () {
		const el = this._element.shadowRoot.querySelector("audio");

		if(el.paused) {
			return this.playTrack({ id: this.__currentTrack });
		}

		this.pause();
	}

	/**
	 * Converts seconds to hh mm ss format
	 *
	 * @param seconds {Number} The seconds value
	 * @returns {string} The hh mm ss formatted value
	 */
	secondsToUserReadableFormat (seconds) {
		const sec_num = parseInt(seconds, 10);
		let _hours = Math.floor(sec_num / 3600);
		let _minutes = Math.floor((sec_num - (_hours * 3600)) / 60);
		let _seconds = sec_num - (_hours * 3600) - (_minutes * 60);
		let construct = '';

		if (_hours < 10) {
			_hours = `0${_hours}`;
		}

		if (_minutes < 10) {
			_minutes = `0${_minutes}`;
		}

		if (_seconds < 10) {
			_seconds = `0${_seconds}`;
		}

		if(_hours !== '00') {
			construct += `${_hours}h `;
		}

		if(_minutes !== '00') {
			construct += `${_minutes}m `;
		}

		return `${construct}${_seconds}s`;
	}

	/**
	 * Play a given song
	 *
	 * @param track {Object} The track object
	 */
	playTrack (track) {
		const that = this;
		const el = this._element.shadowRoot;
		const duration = this.secondsToUserReadableFormat(parseFloat(track.duration));

		let player = el.querySelector("audio");
		let image = el.querySelector("#player-image");
		let info = el.querySelector(".player-info");
		let scrollerContainer = el.querySelector(".scroller-container");
		let play = el.querySelector("#play");

		play.classList.remove("fa-play");
		play.classList.add("fa-pause");

		if(track.id === this.__currentTrack) {
			return player.play();
		}

		this.__globalChannel.trigger("playing:track", this.__currentTrack);

		image.src = track.artwork;
		info.innerText = `${track.artist} - ${track.title}`;

		scrollerContainer.querySelector("#timeEnd").innerText = duration;

		this.__currentTrack = track.id;

		player.src = track.src;
		player.play();
		this.__history.push(track.id);
	}

	/**
	 * Pause the player
	 */
	pause () {
		const el = this._element.shadowRoot;
		const playButton = el.querySelector("#play").classList;

		playButton.add("fa-play");
		playButton.remove("fa-pause");

		el.querySelector("audio").pause();
	}

	shuffle () {
		this.queue = this.__globalChannel.request("api:shuffle:tracks");
	}

	/**
	 * Set the queue
	 *
	 * @param queue {Array} The new queue array
	 */
	set queue (queue) {
		this.__queue = queue;
	}

	/**
	 * Returns the queue array.
	 *
	 * @returns {Array}
	 */
	get queue () {
		return this.__queue;
	}

	/**
	 * Set the player's volume & store the value locally
	 * @param amount
	 */
	set volume (amount) {
		this.__volume = parseFloat(amount);
		this._element.shadowRoot.querySelector("audio").volume = parseFloat(this.__volume);
	}

}

/**
 *  Export the Component
 *
 * @exports Player
 */
export default Player;
