import App from "app/app";
import { View } from "@jakejarrett/marionette-component";
import { attributes } from "marionette-decorators";
import Player from "modules/common/components/Player";
import Template from "./index.html";
import "./index.scss";

/**
 * Top bar
 *
 * @module modules/views/player
 * @exports PlayerView
 */
@attributes({
	template: Template,
	className: "player"
})
class PlayerView extends View {

    /**
     * On render, we want to add the navigation
     *
     * @protected
     */
    onRender () {
		this.registerComponent(App.Compontents, "media-player", Player, this.$el.find("#player-container"), {
			defaultImage: `file://${__dirname}/assets/img/blank_artwork.png`
		});
    }

    onBeforeDestroy () {
        this.clearComponents();
    }

}

export default PlayerView;
