import App from "app/app";
import API from "modules/common/controllers/api-facade"
import { CollectionView } from "backbone.marionette";
import SongView from "../../song/song";
import {attributes, on} from "marionette-decorators";
import Radio from "backbone.radio";
import Template from "./home.html";
import "./home.scss";

/**
 * Home view
 *
 * @module modules/pages/home
 * @exports HomeView
 */
@attributes({
    className: "home",
	childView: SongView
})
class HomeView extends CollectionView {

	template (serializedModel) {
		return _.template(Template)(serializedModel);
	}

    initialize () {
		const that = this;
		this.__globalChannel = Radio.channel("app");
    	this.collection = new Backbone.Collection();
    	this.collection.comparator = (modelA, modelB) => {
    		const artistName1 = `${modelA.get('artist')}`;
    		const artistName2 = `${modelB.get('artist')}`;

    		return artistName1.localeCompare(artistName2);
		};

		this.API = new API();

		this.childViewContainer = ".song-container";

		this.__globalChannel.on("songsLoaded", (options) => {
			that.collection.set(options.songs);
			that.render();
		});

		this.API.initialize();

		/**
		 * Just get the next track for now
		 */
		this.__globalChannel.on("player:song:ended", id => {
			const newId = id + 1;
			const model = this.collection.get(newId);

			this.__globalChannel.trigger("play:track", model.toJSON());
		});

		this.__globalChannel.on("player:next:song", () => {
			const nextSong = this.__globalChannel.request("player:track:current") + 1;
			const model = this.collection.get(nextSong);

			this.__globalChannel.trigger("play:track", model.toJSON());
			this.__globalChannel.trigger("player:song:ended", nextSong - 1);
		});

		this.__globalChannel.on("player:previous:song", () => {
			const previousSong = this.__globalChannel.request("player:track:previous");
			const model = this.collection.get(previousSong);

			this.__globalChannel.trigger("play:track", model.toJSON());
			this.__globalChannel.trigger("player:song:ended", previousSong - 1);
		});

		this.__globalChannel.trigger("change:title", this.API.currentService.title);

		this.__globalChannel.reply("player:amountOfTracks", () => {
			return this.collection.models.length + 1;
		})
	}

}

export default HomeView;
