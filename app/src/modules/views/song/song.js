import App from "app/app";
import API from "modules/common/controllers/api-facade"
import { View } from "backbone.marionette";
import { on } from "marionette-decorators";
import Radio from "backbone.radio";
import Template from "./song.html";
import "./song.scss";

class SongView extends View {

	template (serializedModel) {
		return _.template(Template)(serializedModel);
	}

	initialize (options) {
		this.model = options.model;

		this.__channel = Radio.channel("app");

		this.__channel.on("player:song:ended", id => this.toggleState(id, true));
		this.__channel.on("player:toggle:playback", () => this.toggleState());
	}

	toggleState (id, shouldLookupNext) {
		if (id === undefined) {
			id = this.__channel.request("player:currentTrack");
		}

		const isPlaying = this.el.querySelector(".song").classList.contains("playing");
		const isTrack = id === this.model.get("id");
		const isNextTrack = id + 1 === this.model.get("id");
		const isPreviousTrack = id - 1 === this.model.get("id");

		if (isTrack && isPlaying) {
			this.setAsInactive(false);
		}

		if ((isTrack && !isPlaying) || (isNextTrack && shouldLookupNext) || isPreviousTrack) {
			this.setAsActive(false);
		}
	}

	@on("click .song")
	onPlaySong (event) {
		const isPlaying = event.currentTarget.classList.contains("playing");

		if (!isPlaying) {
			this.setAsActive(true);
		} else {
			this.setAsInactive(true);
		}

	}

	setAsActive (shouldTrigger) {
		const el = this.el.querySelector(".song");
		const icon = el.querySelector(".fa");

		if (shouldTrigger) {
			this.__channel.trigger("play:track", this.model.toJSON());
		}

		el.classList.add("playing");
		icon.classList.remove("fa-play");
		icon.classList.add("fa-pause");
	}

	setAsInactive (shouldTrigger) {
		const el = this.el.querySelector(".song");
		const icon = el.querySelector(".fa");

		if (shouldTrigger) {
			this.__channel.trigger("pause:track");
		}

		el.classList.remove("playing");
		icon.classList.add("fa-play");
		icon.classList.remove("fa-pause");

		this.__channel.off("playbackFinished", _ => this.setAsInactive());
	}

}

export default SongView;
